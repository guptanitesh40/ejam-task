# EJAM-TASK

- This task contains webhook route /order which will allow external CRM to send orders data to it. CRM will pass the same property as the Order model has. If order urlOfSale is not in Domains list or such domain is not active, reject the request. Create new Order in our DB when it is accepted and send WebSocket event "ORDER" including order data ONLY to WebSocket clients who are listening on the same domain as order.urlOfSale


## Getting started
install npm in your machine
recomanded node version : v15.8.0
Take a clone 

## Add Global dependencies

Global dependencies
 - typescript   => npm i -g typescript
 - nodemon      => npm i -g nodemon
 - tsc          => npm i -g tsc

## Installation

 - cd project repo
 - cd npm i  // install dependencies
 - npm run dev // to run a project

