let io: any;

export class Socket {
    
    public init = (httpServer: any) => {
        io = require("socket.io")(httpServer, {
            cors: {
                origin: "http://localhost:3000",
                methods: ["GET", "POST"],
                allowedHeaders: ["*"],
                credentials: true
            }
        });
        return io;
  }
  public getIO = () => {
    if (!io) {
        throw new Error("Socket.io not initialized!");
    }
    return io;
  }
}
