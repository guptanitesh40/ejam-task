import { Document } from 'mongoose';
export default interface iOrder extends Document {
    orderId: string;
    createdAt: Date;
    orderDate: Date;
    productIds: [];
    currency: string;
    price: Float32Array;
    url: string;
    isActive: boolean;
}