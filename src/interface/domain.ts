import { Document, Schema } from 'mongoose';
export default interface iDomain extends Document {
    _id: Schema.Types.ObjectId;
    name: string;
    createdAt: Date;
    isActive: boolean;
}