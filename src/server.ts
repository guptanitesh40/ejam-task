import * as express from 'express';
import * as helmet from 'helmet';
import * as morgan from 'morgan';
import * as compression from 'compression';
import { Routes } from './routes';
import * as mongoose from 'mongoose';
import { config } from 'dotenv';
import { Socket } from './config/socket';
import { DomainController } from './controller/domain';

config();

export class Server {
   
    public app: express.Application
    public socket: Socket = new Socket();
    public domainController: DomainController = new DomainController();

    constructor () {
      // MongoDB connection started
        mongoose
        .connect(
          `mongodb://${process.env.MONGODB_HOST}:${process.env.MONGODB_PORT}/${process.env.MONGODB_DATABASE}`
        )
        .then(() => console.log("Database Connected"))
        .catch((err) => console.log(err));

        this.app = express();
        this.app.use(helmet());
        this.app.use(morgan("dev"));
        this.app.use(compression());
        this.app.use(express.json({ limit: '50mb'}));
        this.app.use(express.urlencoded({extended: true}));

        const routes = new Routes();
        this.app.use("/api", routes.path());
        const server = this.app.listen(process.env.PORT, () => {
            console.log(`Server is started at port : ${process.env.PORT}`);
        });

        const io: any = this.socket.init(server);
        io.use(async (socket: any, next: any) => {
            if (!socket?.handshake?.query?.url) {
              next(new Error('Authentication error'));
            }
            const domain = await this.domainController.findDomain(socket?.handshake?.query?.url);
            if(domain) {
              next();
            }
            else {
               next(new Error('Authentication error'));
            }

        }).on("connection", (socket: any) => {

            socket.join(socket?.handshake?.query?.url);
            
            console.log("Client connected " + socket.id);

            socket.on("disconnect", () => {
                console.log("Client disconnected " + socket.id);    
            })
        });
    }

    public getInstance() {
        return this.app;
    }
}
new Server();