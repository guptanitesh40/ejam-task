import * as express from 'express';
import { OrderRoute } from './routes/order';

export class Routes {

    public path () {
            
        const router = express.Router();
        router.use('/order', OrderRoute);
        return router;
    
    }
}