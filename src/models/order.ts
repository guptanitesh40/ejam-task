import iOrder  from '../interface/order';
import { Schema, model } from "mongoose";
import aggregatePaginate = require("mongoose-aggregate-paginate-v2");
import mongoosePaginate = require("mongoose-paginate-v2");

const OrderSchema: Schema = new Schema(
    {
        _id: Schema.Types.ObjectId,
        orderId: {
            type: String,
            required: true
        },
        orderDate: {
            type: Date,
            required: true
        },
        productIds: {
            type: Array,
            required: true
        },
        currency: {
            type: String,
            required: true
        },
        price: {
            type: Number,
            required: true
        },
        url: {
            type: String,
            required: true
        },
        isActive: {
            type: Boolean,
            required: true
        }
    }, 
    {
        timestamps: true
    }
);

OrderSchema.plugin(aggregatePaginate);
OrderSchema.plugin(mongoosePaginate);

export default model<iOrder>('order', OrderSchema);