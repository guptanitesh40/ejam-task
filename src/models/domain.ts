import iDomain  from '../interface/domain';
import { Schema, model } from "mongoose";

const DomainSchema: Schema = new Schema(
    {
        _id: Schema.Types.ObjectId,
        name: {
            type: String,
            required: true
        },
        isActive: {
            type: Boolean,
            required: true
        }
    }, 
    {
          timestamps: true
    }
);

export default model<iDomain>('domain', DomainSchema);