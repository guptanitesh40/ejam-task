import { Request, Response } from 'express';
import DomainModel from '../models/domain';
export class DomainController {

    public findDomain = async (domain: string) => {
        try {
            return await DomainModel.findOne({ name: domain, isActive:true });   
        } catch (error) {
            return false;
        }
    }

}