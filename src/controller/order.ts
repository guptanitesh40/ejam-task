import { Request, Response } from 'express';
import OrderModel from '../models/order';
import { DomainController } from './domain';
import { Socket } from '../config/socket';
export class OrderController {

    private domainController: DomainController = new DomainController();
    private socket: Socket = new Socket();

    public createOrder = async (req: Request, res: Response) => {
        
        try {
            let domain: any = req.body.url;
            const domainCheck = await this.domainController.findDomain(domain);
            if(!domainCheck) {
                return res.status(500).json({
                    "message": `Invalid url ${domain}`
                });
            }
            
            const result =  await OrderModel.insertMany(req.body);
            this.socket.getIO().to(domain).emit("ORDER", result);
            return res.status(201).json({
                "message": "Products created successfully"
            });
        } catch (error) {
            return res.status(500).json({
                "message": "Error occurred, Please try again later"
            });
        }
    }

    public getOrder = async (req: Request, res: Response) => {
        try {
            const options:object = {
                page: req.query.page,
                limit: 10,
                collation: {
                  locale: 'en',
                },
            };
            const result = await OrderModel.paginate({isActive:true}, options);            
            return res.status(200).json({
                "message": "Products returned successfully",
                result
            });
        } catch (error) {
            return res.status(500).json({
                "message": "Error occurred, Please try again later"
            });
        }
    }

}