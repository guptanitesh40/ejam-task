import { Router } from 'express';
import { OrderController } from '../controller/order';

const router: Router = Router();
const orderController: OrderController = new OrderController();

router.post("/", orderController.createOrder);
router.get("/", orderController.getOrder);

export const OrderRoute: Router = router;